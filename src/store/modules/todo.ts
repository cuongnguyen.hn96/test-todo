import Todo from "../../type/todo";
export default {
    state: () => ({
        todos: [],
        contentSearch: "",
        listCheckBox: []
    }),
    mutations: {
        addTodo(state: any, payload: Todo) {
            state.todos.push(payload)
        },
        setContentSearch(state: any, payload: string) {
            state.contentSearch = payload
        },
        removeTodo(state:any, payload: number) {
            const result = state.todos.findIndex((item: Todo) => item.id === payload)
            state.todos.splice(result,1)
        },
        setListCheckBox(state:any, payload: number) {
            state.listCheckBox.push(payload)
        },
        removeCheckbox(state:any, payload: number) {
            const result = state.listCheckBox.findIndex((item: number) => item === payload)
            state.listCheckBox.splice(result, 1)
        },
        doneTodo(state:any) {
            state.todos.forEach((item: Todo) => {
                item.done = state.listCheckBox.includes(item.id);
            })
        },
        removeTodoByCheckbox(state:any) {
            state.listCheckBox.forEach((id: number) => {
                const result = state.todos.findIndex((item: Todo) => item.id === id)
                state.todos.splice(result, 1)
            })
        },
        resetListCheckBox(state:any) {
          state.listCheckBox = []
        },
        updateTodo(state:any, payload: Todo) {
            const result = state.todos.find((item: Todo) => item.id === payload.id)
            result.name = payload.name;
            result.description = payload.description;
            result.date = payload.date;
            result.priority = payload.priority;
            result.done = payload.done;
        },
        showDetail(state:any, payload: number) {
            state.todos.forEach((item: Todo) => {
                if(item.id === payload) {
                    item.showDetail = !item.showDetail
                }else{
                    item.showDetail = false
                }

            })
        }
    },
    actions: {
        addTodo({commit}: any, payload: any) {
            commit("addTodo", payload);
        },
        setContentSearch({commit}: any, payload: any) {
            commit("setContentSearch", payload);
        },
        removeTodo({commit}: any, payload: any) {
            commit("removeTodo", payload);
        },
        setListCheckBox({commit}: any, payload: any) {
            commit("setListCheckBox", payload)
        },
        removeCheckbox({commit}: any, payload: any) {
            commit("removeCheckbox", payload)
        },
        doneTodo({commit}: any, payload: any) {
            commit("doneTodo", payload)
        },
        removeTodoByCheckbox({commit}: any, payload: any) {
            commit("removeTodoByCheckbox", payload)
        },
        updateTodo({commit}: any, payload: any) {
            commit("updateTodo", payload)
        },
        showDetail({commit}: any, payload: any) {
            commit("showDetail", payload)
        }
    },
    getters: {
        resultTodo (state: any) {
            if(!state.contentSearch){
                return state.todos
            }
            return state.todos.filter((todo: Todo) => todo.name.includes(state.contentSearch))
        }
    }
}
