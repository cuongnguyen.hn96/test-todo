export default interface Todo {
    id: number
    name: string,
    description: string,
    date: string,
    priority: number, // 1 : Low , 2: Normal, 3: High
    done: boolean, // false: In Progress , true: Done
    showDetail: boolean,
}
