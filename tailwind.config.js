module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      height: {
        '256': '64rem',
      },
      boxShadow: {
        'box-custom': '0px 3px 1px -2px rgb(0 0 0 / 20%), 0px 2px 2px 0px rgb(0 0 0 / 14%), 0px 1px 5px 0px rgb(0 0 0 / 12%);'
      },
      maxHeight: {
        '37': '37rem',
      }
    },
  },
  plugins: [],
}
