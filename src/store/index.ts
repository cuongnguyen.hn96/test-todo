import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import Todo from './modules/todo'

const vuexLocal = new VuexPersistence({
  key: 'vuex',
  storage: window.localStorage
})

const store = new Vuex.Store({
  modules: {
    todo: Todo
  },
  plugins: [vuexLocal.plugin]
});

export default store